#!/bin/bash  
# create technical-test namespace
kubectl apply -f technical-test/namespace.yaml --namespace=technical-test
# create memory defaults
kubectl apply -f technical-test/memory-defaults.yaml --namespace technical-test --namespace=technical-test
# create service
kubectl apply -f technical-test/service.yaml --namespace=technical-test
# create a deployment
kubectl apply -f technical-test/deployment.yaml --namespace=technical-test