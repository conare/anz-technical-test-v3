/*
* Test the /version endpoint
*/
var chai = require('../version_app/node_modules/chai/lib/chai');
var chaiHttp = require('../version_app/node_modules/chai-http/lib/http');
var expect = chai.expect;

chai.use(chaiHttp);
describe('version app', function() {
  it('expect /version endpoint to return 200 status code', function(done) {
    chai.request('http://localhost:50000')
    .get('/version')
    .end(function(err, res) {
      expect(res).to.have.status(200);
      done();                               
    });
  });
});