const expect = require('chai').expect;
const nock = require('nock');

const response = [{
    version: "1.0",
    lastcommitsha: "abc57858585",
    description: "pre-interview technical test"
  }];
describe('get app version', () => {
  //mock http request/response
  beforeEach(() => {
    nock('http://127.0.0.1:8080')
      .get('/version')
      .reply(200, response);
  });

  it('returns app version',  () => {
    expect(Array.isArray(response)).to.equal(true)
    expect(response).to.have.length.above(0)
    //check version, lastcommitsha and description
    expect(response[0].version).to.equal('1.0')
    expect(response[0].lastcommitsha).to.equal('abc57858585')
    expect(response[0].description).to.equal('pre-interview technical test')
  });
});