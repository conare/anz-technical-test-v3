#!/bin/bash
# get short git commit sha
export CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
export IMAGE_NAME=version-app
export GCP_PROJECT_ID=conare-sandbox
# set app version
export VERSION=$(cat version.info)
export PORT=8080
# build image locally and tag 
docker build --no-cache --build-arg VERSION=${VERSION} --build-arg LASTCOMMITSHA=${CI_COMMIT_SHORT_SHA} \
--build-arg PORT=${PORT} -t ${IMAGE_NAME}:${VERSION}-${CI_COMMIT_SHORT_SHA} -t gcr.io/${GCP_PROJECT_ID}/${IMAGE_NAME}:${VERSION}-${CI_COMMIT_SHORT_SHA} .