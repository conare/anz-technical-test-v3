var express = require('express');
var app = express();

var port = process.env.PORT;

// api version info
var version = process.env.VERSION;
var lastGitSha = process.env.LASTGITSHA;
var description = 'pre-interview technical test';

// return app vesrion info when requested
app.get('/version', function (req, res) {
  res.status(200).send(
    {
      myapplication: [{
        version: `${version}`,
        lastcommitsha: `${lastGitSha}`,
        description: `${description}`
      }]
    });
  }
);
var server = app.listen(port, function () {
   var port = server.address().port
   
   console.log(`running at port ${port}`);
})
