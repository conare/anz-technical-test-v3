# ANZ Technical Test v3
## Pre-requisites
+ Docker 17.05+
+ Google Cloud SDK
+ Bash 
+ Kubernetes

## Test 1 - Version App
### Requirements
+ Build an app with a single /version endpoint. 
+ Version Application
+ Containerise the application
+ Test Application
+ Create a CI pipeline with automatic git triggers

### Environment Variable
+ GCP_PROJECT_ID - Google Cloud Project ID
+ SHORT_GIT_COMMIT_SHA - The current git commit hash
+ VERSION - Application version Number
+ IMAGE_NAME - Name of the image

### Version Application
The solution has been implemented using node v12+ and express Js

### Continuous Integration Pipeline
The application is built, tested and versioned as a docker image and then pushed to [Google Containter Registry](https://cloud.google.com/container-registry/)

Steps:
+ Install node packages required for the application
+ Run eslint to check for any linting errors
+ Run nock/chai tests to mock api endpoint and validate api output
+ Turn on repository vulnerability scanning to check for known security in docker images pushed to gcr

#### Build
The application is build inside a docker container
##### Local Build
Build locally by running [build.sh](test1/build.sh) script as shown below
```
| ~/repo/kasna/anz-technical-test-v3/test1 @ Chriss-MacBook-Pro (conare) 
| => ./build.sh 
```
#### Gitlab CI Pipeline
At the root of this repository, there is a `.gitlab-ci.yml` which defines a [Gitlab CI](https://docs.gitlab.com/ee/ci/README.html) pipeline that will be executed on each commit. This builds the image and pushes it to [Google Containter Registry](https://cloud.google.com/container-registry/). It also runs units tests against a local live /version endpoint.
The pipeline runs automatically when a change is committed on the master branch. 

#### Linting
Application linting is handled using [eslint](https://eslint.org/)
Eslint config in [eslint directory](test1/eslint). The config outlines expected code syntax for NodeJs and ExpressJs. It covers areas such as indentation, linebreak. More robust rules can be added as the application grows

Linting output when building the container in docker

```
Step 17/28 : RUN npm run lint -- --fix
 ---> Running in 2ceca1907d5e

> myapplication@1.0.0 lint /app
> eslint app.js "--fix"
```

#### Testing
Application testing is done using [mocha](https://mochajs.org/), [chai](https://www.chaijs.com/) and [nock](https://github.com/nock/nock). 

##### Mock Tests
Using nock, we are mocking the api http request/response since we know the expected output before we create the application. The tests are defined under the [tests directory](test1/tests/app.test.js)

Test output when building and testing the application

```
Step 18/28 : RUN npm run test
 ---> Running in 18e89c16570f

> myapplication@1.0.0 test /app
> mocha tests



  get app version
    ✓ returns app version


  1 passing (25ms)
```

##### Unit Testing
Unit tests are defined using chai/mocha. They test the response of the api endpoint /version to make sure the response from the api matches the test rules. The tests are defined under the [tests directory](test1/tests/spec.js). This have been turned off from the pipeline but ran locally.

To run unit tests locally, follow the following steps:
+ run the app using `docker run -p 50000:8080 -d version-app:1.0.12-4d1ed2e`
+ nagivate to [version_app](test1/version_app)
+ run `npm install` to install node packages
+ run `npm test:unit` to run unit tests

The results should be similar to the following

```
> mocha ../tests/spec.js



  version app
    ✓ expect /version endpoint to return 200 status code


  1 passing (21ms)
  ```

**Unit tests have been turn off from the pipeline due to gitlab being unable to run a docker image in docker correctly**

#### Versioning
Versioning is done by tagging the docker image with the application version number and the last git commit SHA. This ensures that each docker container build is unique and corresponds to the application git commit SHA. In addition, this is built inside the application during the CI process by passing the version and Gitcommitsha details arguments to the Docker image as defined in [build.sh](tests/build.sh)

### Local Deployment of Application
Deploy the application locally by running:

```
docker run -d --name version-app -p 3000:8080 version-app:1.0.0-51cbf1d
```
Assuming your built image is tagged as `version-app:1.0.0-51cbf1d`

Check for api endpoint by browsing to `http://127.0.0.1:3000/version`

### Limitations
+ Unit tests. Create a dedicated slave for gitlab-ci (gke cluster)


## Test 2
Continuous deployment is implemented using GKE Cluster
### Assumptions
+ Created kubernetes manifests for running the cluster locally. 
+ Deployments will be done locally 

### Creating a GKE Cluster

Run the following command to create a `technical-test-cluster` in `asia-east1` with nodes running in all zones for high availability of the application

```
gcloud beta container --project "conare-sandbox" clusters create "technical-test-cluster" --region "asia-east1" --no-enable-basic-auth --cluster-version "1.13.11-gke.14" --machine-type "n1-standard-1" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "1" --enable-cloud-logging --enable-cloud-monitoring --enable-ip-alias --network "projects/conare-sandbox/global/networks/network-1" --subnetwork "projects/conare-sandbox/regions/asia-east1/subnetworks/subnet-1" --default-max-pods-per-node "110" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair

```
You should be able to see the nodes by running `kubectl get nodes`
```
| => kubectl get nodes
NAME                                                  STATUS   ROLES    AGE    VERSION
gke-technical-test-clust-default-pool-4440fe72-qq06   Ready    <none>   5m3s   v1.13.11-gke.14
gke-technical-test-clust-default-pool-8f23e19e-3jlj   Ready    <none>   5m2s   v1.13.11-gke.14
gke-technical-test-clust-default-pool-e8161947-2n0f   Ready    <none>   5m1s   v1.13.11-gke.14
```
### Deploying

#### Deploying Locally
Deploy the application by running the [deployment script](test2/deploy.sh) as shown below.
```
| => ./deploy.sh 
namespace/technical-test created
limitrange/limit-mem-cpu-per-container created
service/technical-test-service created
deployment.apps/technical-test-deployment created
```
You should be able to see the pods by running
```
| => kubectl get pods --namespace=technical-test
NAME                                         READY   STATUS    RESTARTS   AGE
technical-test-deployment-6685f7b568-7nv75   1/1     Running   0          10m
technical-test-deployment-6685f7b568-njqqn   1/1     Running   0          10m
technical-test-deployment-6685f7b568-s5fb8   1/1     Running   0          10m
```
#### Deploying to production
In order to deploy to higher environments (staging, preprod and prod), we need to make some changes to make sure that both the application and app nodes can scale.
Steps:
+ Use [kustomize](https://kubernetes.io/blog/2018/05/29/introducing-kustomize-template-free-configuration-customization-for-kubernetes/) to dynamically generate kubernetes manifests for different environments with different values. For instance, in [deployment.yaml](test2/technical-test/deployment.yaml), the `replicas and image` needs to be updated dynamically based on the environment. 
+ Turn on [HorizontalPodAutoscaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#support-for-horizontal-pod-autoscaler-in-kubectl)
+ Turn on [Cluster Autoscaling](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-autoscaler) to scale
+ Update [service.yaml manifest](test2/technical-test/service.yaml) Spec [Type Loadbalancer](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/) to create an external load balancer
+ Enable Http load balancing
+ Run load tests to test how efficiently the application scales
+ Use gitlab-ci or Jenkinsfile to add steps to deploy to higher environments with automated deployment to dev environment and manual promotion gates to test, staging and production